package com.agslinda.barndoorclock;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.view.View.*;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import java.util.*;
import android.graphics.drawable.*;
import android.media.*;
import android.provider.*;

public class MainActivity extends Activity {

	private Intent intent = new Intent(Intent.ACTION_VIEW);
	private int sleep = 0;
	public static boolean clockRunning = true;
	private Button restButton;
	private Button runButton;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
	{
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		forcePrecisionTimer();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		
		StringBuilder sb = new StringBuilder();
		sb.append("Tangent error: 0.0% | Time elapsed: 0s | Angle: 0°");
		
		final ImageView clock = (ImageView)findViewById(R.id.clock);

		final TextView readOut = (TextView)findViewById(R.id.readOut);
		String txt = sb.toString();
		
		final EditText rpm = (EditText)findViewById(R.id.rpm);
		final EditText initialAngle = (EditText)findViewById(R.id.initialAngle);
		final EditText tocks = (EditText)findViewById(R.id.tocks);
		
		readOut.setText(txt);

		runButton = (Button)findViewById(R.id.run_button);
		runButton.setText("Start");
		final MainActivity thiss = this;
		runButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					String t = runButton.getText().toString();
					if ("Start".equals(t)) {
						int tockNum = getTocks(tocks);
						double initialAngleNum = getInitialAngle(initialAngle);
						double rpmNum = getRpm(rpm);
						
						elapsed = 0;
						createUpdater(thiss, readOut, clock, initialAngleNum, rpmNum, tockNum);
						pressButton(runButton, "Stop");
						clockRunning = true;
					} else {
						unpressButton(runButton, "Start");
						clockRunning = false;
					}
					
				}
				
				private double getRpm(EditText rpm) {
					String txt = rpm.getText().toString();
					double result = 60.0;
					if (!txt.isEmpty()) {
						try {
							String s = txt.replaceAll(" .*", "").replaceAll("[^0-9\\.]", "");
							result = Double.parseDouble(s);
							if (result < 15.0) {
								result = 15.0;
							}
						} catch (Throwable ignore) {}

					}
					
					rpm.setText(result + " secs per cycle @ 0°");
					return result;
				}

				private double getInitialAngle(EditText initialAngle) {
					String txt = initialAngle.getText().toString();
					double result = 3.0;
					if (!txt.isEmpty()) {
						try {
							String s = txt.replaceAll(" .*", "").replaceAll("[^0-9\\.]", "");
							result = Double.parseDouble(s);
							if (result > 45.0) {
								result = 45.0;
							}
						} catch (Throwable ignore) {}
						
					}
					initialAngle.setText(result + "° initial angle");
					return result;
				}
				
				private int getTocks(EditText tocks) {
					String txt = tocks.getText().toString();
					int result = 0;
					if (!txt.isEmpty()) {
						String s = txt.replaceAll(" .*", "").replaceAll("[^0-9]", "");
						result = Integer.parseInt(s);
					}
					result = result == 2 || result == 4
						|| result == 8 || result == 16 || result == 32
						? result : 16;
					tocks.setText(result + " tocks per cycle");
					return result;
				}
			});
			
			restButton = (Button)findViewById(R.id.rest_button);
			restButton.setText("Rest");
		restButton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View p1) {
					sleep += 2;
					Toast toast = Toast.makeText(thiss.getApplicationContext(), "Dial two cycles ahead", Toast.LENGTH_LONG);
					toast.show();
					pressButton(restButton, sleep + " Rest Remaining");
				}

				
			});
  	}
	
	public static void forcePrecisionTimer() {
        new Thread() {
            {
                setDaemon(true);
                start();
            }

            public void run() {
                //noinspection InfiniteLoopStatement
                while (true) {
                    try {
                        Thread.sleep(Long.MAX_VALUE);
                    } catch (Exception ex) {}
                }
            }
        };
    }

	private void createUpdater(final MainActivity activity, final TextView readOut, final ImageView clock,
		double initialAngle, double rpm, int tocks) {
		AsyncTask<Void, Void, Integer> updater = new Updater(activity, readOut, clock, 0, initialAngle, rpm, tocks);
		updater.execute();
	}
	
	private static double elapsed = 0.0;
	
	public static void putTimeElapsed(double time) {
		elapsed = time;
	}
	
	public static double getSecondsElapsed() {
		return elapsed / 1000.0;
	}
	
	public void setSleep(int sleep) {
		this.sleep = sleep;
	}
	
	public int getSleep() {
		return sleep;
	}
	
	public Button getRestButton() {
		return restButton;
	}
	
	public Button getRunButton() {
		return runButton;
	}
	
	public static void pressButton(Button b, String s) {
		b.setText(s);
		b.setBackgroundColor(0xFF300000);
	}
	
	public static void unpressButton(Button b, String s) {
		b.setText(s);
		b.setBackgroundColor(0xFF800000);
	}

}
