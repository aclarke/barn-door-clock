package com.agslinda.barndoorclock;
import android.os.*;
import android.widget.*;
import android.media.*;
import android.graphics.drawable.*;
import android.app.*;
import android.provider.*;

//comment

public class Updater extends AsyncTask<Void, Void, Integer> {
	private final double initialAngle;
	private int step = 0;
	private double angle;
	private double totalTime = 0.0;
	private long startTime = 0;
	private final TextView readOut;
	private final ImageView clock;
	private final MainActivity activity;
	public final double RAD_CONVERSION = Math.PI / 180.0;
	public final double HOUR_ANGLE = (360.0 + (360.0 / 365.2422)) / 24.0;
	public final double CYCLE_ANGLE;
	public final double CYCLE_LENGTH;
	public final int STEPPER_MOTOR;
	public final double MINUTE_STEPPER_ANGLE;
	public final double MINUTE_STEPPER_LENGTH;
	public final int STEPPER_MILLIS;
	private double ratio = 1.0;
	private int FAST_FORWARD = 1;

	private final double rpm;

	public Updater(MainActivity activity, TextView readOut, ImageView clock, int step,
		double initialAngle, double rpm, int tocks) {
		this.clock = clock;
		this.readOut = readOut;
		this.step = step;
		this.activity = activity;
		this.initialAngle = initialAngle;
		angle = initialAngle;
		this.rpm = rpm;
		STEPPER_MOTOR = tocks;
		int cycle = (int) (60000 * rpm / 60.0);
		CYCLE_ANGLE = HOUR_ANGLE * rpm / 60.0 / 60.0;
		CYCLE_LENGTH = Math.tan(CYCLE_ANGLE * RAD_CONVERSION);
		STEPPER_MILLIS = cycle / STEPPER_MOTOR;
		MINUTE_STEPPER_ANGLE = CYCLE_ANGLE / STEPPER_MOTOR;
		MINUTE_STEPPER_LENGTH = Math.tan(MINUTE_STEPPER_ANGLE * RAD_CONVERSION);
	}

	@Override
	protected Integer doInBackground(Void[] p1) {
		long currentTimeMillis = System.currentTimeMillis();
		if (startTime == 0) {
			startTime = currentTimeMillis;
		}
		long elapsed = currentTimeMillis - startTime;
		double oldAngle = (HOUR_ANGLE * elapsed / 3600 / 1000) + initialAngle;
		angle = oldAngle + MINUTE_STEPPER_ANGLE;
		double oldLength = Math.tan(oldAngle * RAD_CONVERSION);
		double newLength = Math.tan(angle * RAD_CONVERSION);
		double deltaLength = newLength - oldLength;
		ratio = deltaLength / MINUTE_STEPPER_LENGTH;
		double time = STEPPER_MILLIS / ratio;
		totalTime += time;
		int actualSleepTime = (int) (totalTime - elapsed);
		MainActivity.putTimeElapsed(totalTime);
		if (actualSleepTime < 20) {
			return -1;
		}
		try {
			Thread.sleep(actualSleepTime / FAST_FORWARD);
		}
		catch (InterruptedException e) {}
		step = (step + 1) % STEPPER_MOTOR;
		return step;
	}

	@Override
	protected void onPostExecute(Integer s) {
		if (s < 0) {
			Toast toast = Toast.makeText(activity.getApplicationContext(), "Clock too fast, aborting.", Toast.LENGTH_LONG);
			toast.show();
			MainActivity.unpressButton(activity.getRunButton(), "Start");
			MainActivity.clockRunning = false;
			return;
		}
		StringBuilder sb = new StringBuilder();
		int x = 32 / STEPPER_MOTOR;
		int y = (s * x) + 1;
		int snooze = activity.getSleep();
		
		if (s == 0 && snooze == 1) {
			MediaPlayer mediaPlayer = MediaPlayer.create(activity.getApplicationContext(), Settings.System.DEFAULT_NOTIFICATION_URI);
			mediaPlayer.start();
		}
		String restText = "Rest 2 Cycles";
		if (s == 1 && snooze > 0) {
			snooze -= 1;
			activity.setSleep(snooze);
		}
		Button restButton = activity.getRestButton();
		if (snooze > 0) {
			MainActivity.pressButton(restButton, snooze + " Rest Remaining");
		} else {
			MainActivity.unpressButton(restButton, "Rest 2 Cycles");
		}

		String uri = "drawable/c" + y; 
		
		int imageResource = activity.getResources().getIdentifier(uri, null, activity.getPackageName());
		Drawable image = activity.getResources().getDrawable(imageResource);
		clock.setImageDrawable(image);
		String ratioStr = simpleFormat(ratio, 1.0, 100.0);
		sb.append("Tangent error: " + ratioStr + "% | ");
		sb.append("Time elapsed: " + (int)MainActivity.getSecondsElapsed()).append("s | ");
		sb.append("Angle: " + simpleFormat(angle, 0.0, 1.0)).append("°");
		readOut.setText(sb);
		if (MainActivity.clockRunning) {
			Updater updater = new Updater(activity, readOut, clock, s, initialAngle, rpm, STEPPER_MOTOR);
			updater.startTime = startTime;
			updater.totalTime = totalTime;
			updater.ratio = ratio;
			updater.angle = angle;
			updater.execute();
		}
	}

	private String simpleFormat(double num, double rebase, double factor) {
		return num < rebase + 0.00001 ? "0.000" : (((num - rebase) * factor) + "0000").substring(0, 6);
	}
}
